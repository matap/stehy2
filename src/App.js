import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import { Switch, Route, Redirect } from 'react-router-dom';
import qs from 'qs';
import { Spinner } from '@chevtek/react-spinners';
import Metadata from './Components/Metadata';
import Sidebar from './Components/Sidebar';

import Footer from './Components/Footer.js';
import InstructionsPage from './Pages/Instructions';
import InstructionPage from './Pages/Instruction';
import TextPage from './Pages/TextPage';
import LocationMapFilter from './Pages/LocationMapFilter';

import $ from 'jquery';

import {
  selectedProjectCookieName,
  projectConfigurationPath
} from './Utilities/SelectedProject';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: decodeURI(window.location.hash.substr(1)),
      showSidebar: $(window).width() >= 820 ? true : false,
      // $(window).width() >= 820
      //   ? true
      //   : window.location.href.indexOf('lokace') >= 1
      //   ? false
      //   : true,
      locationText: ''
    };

    this.onShowChange = this.onShowChange.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
    this.progressBarValue = React.createRef();
    this.onMenuClick = this.onMenuClick.bind(this);
  }

  onUpdate(val) {
    this.setState({
      searchText: val,
      locationText: ''
    });
  }

  onShowChange(val) {
    this.setState({
      showSidebar: val
    });
  }

  onMenuClick(a) {
    if ($(window).width() < 820) {
      this.setState({
        showSidebar: false,
        locationText: ''
      });
    }
  }

  render() {
    const projectId = '975bf280-fd91-488c-994c-2f04416e5ee3'; // this.props.cookies.get(selectedProjectCookieName);
    if (!projectId) {
      return <Redirect to={projectConfigurationPath} />;
    }
    var isChrome =
      /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    const { language, changeLanguage, location } = this.props;
    var openClass = this.state.showSidebar ? '' : 'full';

    return (
      <div className={isChrome ? 'chrome' : ''}>
        <div className="top-border" />
        <Sidebar
          showLocation={true}
          language={language}
          changeLanguage={changeLanguage}
          showSidebar={this.state.showSidebar}
          onUpdate={this.onUpdate}
          onMenuClick={this.onMenuClick}
          onShowChange={this.onShowChange}
          {...this.props}
        />
        <div className={openClass + ' main-content-container'}>
          <div className="fluid-container main-content-container-inner">
            <div className="row">
              <div className="col-xs-12">
                <Switch>
                  <Route
                    path="/:lang?/navod-:instructionSlug"
                    render={matchProps => (
                      <InstructionPage
                        {...matchProps}
                        language={language}
                        spinner={this.progressBarValue}
                      />
                    )}
                  />
                  <Route
                    path="/:lang?/instruction-:instructionSlug"
                    render={matchProps => (
                      <InstructionPage
                        {...matchProps}
                        language={language}
                        spinner={this.progressBarValue}
                      />
                    )}
                  />
                  <Route
                    exact
                    path="/:lang?/o_projektu"
                    render={matchProps => (
                      <TextPage {...matchProps} language={language} />
                    )}
                  />
                  <Route
                    exact
                    path="/:lang?/vysivky-zenskych-rukavcu"
                    render={matchProps => (
                      <TextPage {...matchProps} language={language} />
                    )}
                  />
                  <Route
                    exact
                    path="/:lang?/postup-:urlslug"
                    render={matchProps => (
                      <TextPage {...matchProps} language={language} />
                    )}
                  />
                  <Route
                    exact
                    path="/:lang?/about-project"
                    render={matchProps => (
                      <TextPage {...matchProps} language={language} />
                    )}
                  />
                  <Route
                    exact
                    path="/:lang?/stehy_podle_mista"
                    render={matchProps => (
                      <LocationMapFilter
                        {...matchProps}
                        language={language}
                        onLocationClick={this.onMenuClick}
                      />
                    )}
                  />

                  <Route
                    path="/:lang?/lokace-:location"
                    render={matchProps => (
                      <InstructionsPage {...matchProps} language={language} />
                    )}
                  />
                  <Route
                    key={this.props.location.pathname}
                    path="/:lang?/hledej/:searchText"
                    render={matchProps => (
                      <InstructionsPage {...matchProps} language={language} />
                    )}
                  />
                  <Route
                    key={this.props.location.pathname}
                    path="/:lang?/:instructionsSlug"
                    render={matchProps => (
                      <InstructionsPage {...matchProps} language={language} />
                    )}
                  />

                  <Route
                    key={this.props.location.pathname}
                    path="/:lang"
                    render={matchProps => (
                      <InstructionsPage {...matchProps} language={language} />
                    )}
                  />
                  <Route
                    key={this.props.location.pathname}
                    path="/"
                    render={matchProps => (
                      <InstructionsPage {...matchProps} language={language} />
                    )}
                  />
                  <Route
                    path="*"
                    render={() => {
                      return <Redirect to="/" push />;
                    }}
                  />
                </Switch>
              </div>
            </div>
          </div>
          <Spinner name="apiSpinner">
            <div className="progress-bar">
              <div className="loader" />
              <div className="progress-bar-value" ref={this.progressBarValue} />
            </div>
          </Spinner>
          <Footer language={language} />
        </div>
        <div className="clearfix" />
      </div>
    );
  }
}

export default withCookies(App);
