import * as HomeStore from '../Stores/Home';
import * as InstructionStore from '../Stores/Instruction';
import * as TaxonomyStore from '../Stores/Taxonomy';

const allStores = [HomeStore, InstructionStore, TaxonomyStore];

const resetStores = () => allStores.forEach(store => store.resetStore());

export { resetStores };
