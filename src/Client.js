import Cookies from 'universal-cookie';
import {
  selectedProjectCookieName,
  defaultProjectId
} from './Utilities/SelectedProject';

// kentico cloud
import { DeliveryClient, TypeResolver } from 'kentico-cloud-delivery';

// models
import { AboutUs } from './Models/about_us';
import { Accessory } from './Models/accessory';
import { Grinder } from './Models/grinder';
import { HeroUnit } from './Models/hero_unit';
import { Home } from './Models/home';
import { Office } from './Models/office';
import { Tweet } from './Models/tweet';
import { Instruction } from './Models/instruction';
import { Taxonomy } from './Models/taxonomy';
import { Text } from './Models/text';

const projectId = '3781e195-e3f8-0093-2cbe-eb8f8d5b0fbf';
const previewApiKey =
  'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiZDNkZDE2YmJiYjY0OWQ3YWNhZmUwOGI1NGE1YTVhMSIsImlhdCI6IjE1NDMyNjc1NDEiLCJleHAiOiIxODg4ODY3NTQxIiwicHJvamVjdF9pZCI6IjM3ODFlMTk1ZTNmODAwOTMyY2JlZWI4ZjhkNWIwZmJmIiwidmVyIjoiMS4wLjAiLCJhdWQiOiJwcmV2aWV3LmRlbGl2ZXIua2VudGljb2Nsb3VkLmNvbSJ9.1a7D_hyeIN_EyDZ_vfot0WHH_OWqaeUk3Hjqkaulaz0';

// configure type resolvers
let typeResolvers = [
  new TypeResolver('about_us', () => new AboutUs()),
  new TypeResolver('accessory', () => new Accessory()),
  new TypeResolver('grinder', () => new Grinder()),
  new TypeResolver('hero_unit', () => new HeroUnit()),
  new TypeResolver('home', () => new Home()),
  new TypeResolver('office', () => new Office()),
  new TypeResolver('tweet', () => new Tweet()),
  new TypeResolver('taxonomy', () => new Taxonomy()),

  new TypeResolver('instruction', () => new Instruction()),
  new TypeResolver('text', () => new Text())
];

const cookies = new Cookies(document.cookies);
let currentProjectId = projectId || cookies.get(selectedProjectCookieName);
if (currentProjectId) {
  cookies.set(selectedProjectCookieName, currentProjectId, { path: '/' });
} else {
  currentProjectId = defaultProjectId;
}

const isPreview = () => false; //previewApiKey !== '';

let Client = new DeliveryClient({
  projectId: currentProjectId,
  typeResolvers: typeResolvers,
  previewApiKey: previewApiKey,
  enablePreviewMode: isPreview()
});

const resetClient = newProjectId => {
  Client = new DeliveryClient({
    projectId: newProjectId,
    typeResolvers: typeResolvers,
    previewApiKey: previewApiKey,
    enablePreviewMode: isPreview()
  });
  const cookies = new Cookies(document.cookies);
  cookies.set(selectedProjectCookieName, newProjectId, { path: '/' });
};

export { Client, resetClient };
