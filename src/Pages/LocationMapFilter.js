import $ from 'jquery';
import jQuery from 'jquery';
import React, { Component } from 'react';
import LocationMenu from '../Components/LocationMenu';
import MapImage from '../Images/mapa.jpg';
window.$ = window.jQuery = jQuery;
//import ImageMapper from 'react-image-mapper';
require('jquery-imagemapster'); // must be require, because babel change order of import (window.$ = window.jQuery=jQuery;)

class LocationMapFilter extends Component {
  constructor(props) {
    super(props);
    this.onLocationMenuClick = this.onLocationMenuClick.bind(this);
    this.initMapster = this.initMapster.bind(this);
  }

  componentDidMount() {
    this.initMapster();
  }

  initMapster() {
    var self = this;
    $('.map')
      .mapster('unbind')
      .mapster({
        fillColor: 'ff0000',
        fillOpacity: 0.3,
        boundList: $('.map-menu').find('li a'),
        mapKey: 'data-key',
        listKey: 'data-key',
        onClick: function(data) {
          self.onLocationMenuClick(data.key.toLowerCase());
        }
      });
  }

  onLocationMenuClick(a) {
    this.props.onLocationClick(a);
    this.props.history.push(`/${this.props.language}/lokace-${a}`);
    //this.props.onLocationMenuClick(a);
  }
  // <area data-key="Hornacko" alt="Horňácko" title="Horňácko" href="#" shape="poly" coords="1,906,183,799,304,781,321,809,534,847,660,1086,641,1100,630,1108,609,1106,580,1109,555,1120,540,1132,527,1142,2,1142" />
  render() {
    return (
      <div>
        <h1 className="text-center main-title">Filtrace stehů podle místa</h1>
        <div className="contact">
          <div className="map-container">
            <img
              src={MapImage}
              alt="Muzeum J. A. Komenského"
              className="map"
              useMap="#mymap"
            />
            <map name="mymap">
              <area
                data-key="Banovsko"
                alt="Bánovsko"
                title="Bánovsko"
                href="#"
                shape="poly"
                coords="537,427,699,343,941,551,653,741"
              />
              <area
                data-key="Nivnicko"
                alt="Nivnicko"
                title="Nivnicko"
                href="#"
                shape="poly"
                coords="143,528,332,425,533,429,653,740,535,847,320,808"
              />
              <area
                data-key="Vlcnov"
                alt="Vlčnov"
                title="Vlčnov"
                href="#"
                shape="poly"
                coords="143,523,170,296,275,270,329,424"
              />
              <area
                data-key="uherskobrodsko"
                alt="Uherskobrodsko"
                title="Uherskobrodsko"
                href="#"
                shape="poly"
                coords="279,269,384,182,299,64,396,0,531,1,800,255,702,345,535,428,332,423"
              />
              <area
                data-key="hradcovicko___hradcovsko"
                alt="Hradčovsko"
                title="Hradčovsko"
                href="#"
                shape="poly"
                coords="112,195,295,64,378,181,275,266,169,294"
              />
              <area
                data-key="strani"
                alt="Strání"
                title="Strání"
                href="#"
                shape="poly"
                coords="538,849,653,744,778,951,760,976,753,988,749,1003,739,1017,730,1027,719,1030,708,1030,701,1032,690,1037,684,1046,681,1053,677,1062,671,1071,666,1078,662,1085,658,1090"
              />
              <area
                data-key="bojkovicko___bojkovsko"
                alt="Bojkovsko"
                title="Bojkovsko"
                href="#"
                shape="poly"
                coords="675,135,785,70,894,115,988,2,1395,0,1423,43,1227,377,980,586,948,554,705,346,806,255"
              />
              <area
                data-key="moravske_kopanice"
                alt="Moravské Kopanice"
                title="Moravské Kopanice"
                href="#"
                shape="poly"
                coords="980,586,1227,380,1229,374,1319,371,1318,390,1319,398,1320,415,1318,425,1317,437,1314,453,1308,462,1303,475,1296,487,1292,498,1290,508,1286,517,1284,528,1283,537,1281,546,1281,557,1280,562,1280,569,1283,573,1283,581,1283,587,1283,594,1281,599,1278,608,1275,617,1275,620,1273,625,1271,633,1266,646,1260,665,1250,679,1243,694,1239,718,1234,742,1228,757,1217,772,1197,783,1177,795,1168,797,1154,791,1139,780,1123,773,1113,776,1101,781,1084,784,1049,781,1013,777,973,780,919,783,1032,674"
              />
              <area
                data-key="brezova__lopenik"
                alt="Březovsko"
                title="Březovsko"
                href="#"
                shape="poly"
                coords="652,743,947,555,976,584,1034,674,923,783,897,784,883,786,880,791,883,807,885,817,888,827,888,832,884,836,883,840,874,850,855,856,836,868,823,887,811,904,797,920,789,935,779,951"
              />
              <area
                data-key="uherskoostrozsko"
                alt="Uherskoostrožsko"
                title="Uherskoostrožsko"
                href="#"
                shape="poly"
                coords="0,904,0,340,9,332,158,384,146,525,229,660,157,718,181,796"
              />
              <area
                data-key="borsice_u_blatnice"
                alt="Boršice u Blatnice"
                title="Boršice u Blatnice"
                href="#"
                shape="poly"
                coords="183,794,159,718,229,663,302,780"
              />
              <area
                data-key="uherskohradistsko"
                alt="Uherskohradišťsko"
                title="Uherskohradišťsko"
                href="#"
                shape="poly"
                coords="1,54,202,132,112,194,173,297,161,386,2,334"
              />
            </map>
            <div className="map-menu">
              <LocationMenu onLocationMenuClick={this.onLocationMenuClick} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LocationMapFilter;
