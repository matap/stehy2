import React, { Component } from 'react';
import { translate } from 'react-translate';
import { InstructionStore } from '../Stores/Instruction';
import Link from '../Components/LowerCaseUrlLink';
import dateFormat from 'dateformat';
import { Spinner } from '@chevtek/react-spinners';

import { dateFormats } from '../Utilities/LanguageCodes';

let instructionCount = 1000;

let getState = props => {
  return {
    instructions: InstructionStore.getInstructions(
      instructionCount,
      props.language,
      props.match.params.instructionsSlug,
      props.match.params.searchText
    )
    //pathname: '' //props.location.pathname
  };
};

let getURLLastSegment = props => {
  var lastSegment = window.location.pathname
    .substr(window.location.pathname.lastIndexOf('/') + 1)
    .replace('/', '')
    .replace('en-us', '')
    .replace('cs-cz', '');

  return lastSegment;
};

class Instructions extends Component {
  constructor(props) {
    super(props);

    this.state = getState(props);
    this.onChange = this.onChange.bind(this);
    this.getMainTitle = this.getMainTitle.bind(this);
    dateFormat.i18n = dateFormats[props.language] || dateFormats[0];
  }

  componentDidMount() {
    this.setState(() => {
      return { instructions: null };
    });
    InstructionStore.addChangeListener(this.onChange);
    InstructionStore.provideInstructions(
      this.props.language,
      this.props.match.params.instructionsSlug,
      this.props.match.params.location,
      this.props.match.params.searchText
    );
  }

  componentWillUnmount() {
    InstructionStore.removeChangeListener(this.onChange);
    InstructionStore.unsubscribe();
  }

  // componentWillReceiveProps(nextProps) {
  //     InstructionStore.provideInstructions(nextProps.language, nextProps.match.params.instructionsSlug, nextProps.match.params.searchText);
  //     if (nextProps.startTime !== this.state.startTime) {
  //       this.setState({ startTime: nextProps.startTime });
  //     }
  // }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      prevState.language !== nextProps.language ||
      prevState.pathname !== nextProps.location.pathname
    ) {
      //InstructionStore.provideInstructions(nextProps.language, nextProps.match.params.instructionsSlug);
      dateFormat.i18n = dateFormats[nextProps.language] || dateFormats[0];
      return {
        language: nextProps.language,
        pathname: nextProps.location.pathname
      };
    }
    return null;
  }

  onChange() {
    this.setState(getState(this.props));
  }

  getMainTitle() {
    if (typeof this.props.match.params.location !== 'undefined') {
      if (
        typeof this.state.instructions[0] !== 'undefined' &&
        typeof this.state.instructions[0].location !== 'undefined'
      ) {
        var firstItem = this.state.instructions[0].location.value.filter(
          a => a.codename == this.props.match.params.location
        )[0];
        if (typeof firstItem !== 'undefined') {
          return firstItem.name;
        }
      }
    } else {
      if (
        typeof this.state.instructions[0] !== 'undefined' &&
        typeof this.state.instructions[0].sitemap !== 'undefined'
      ) {
        var firstItem = this.state.instructions[0].sitemap.value.filter(
          a => a.codename == getURLLastSegment()
        )[0];
        if (typeof firstItem !== 'undefined') {
          return this.props.t(firstItem.codename);
          return firstItem.name;
        }
      }
    }

    let searchText = this.props.match.params.searchText;
    if (typeof searchText !== 'undefined' && searchText.length > 0) {
      return this.props.t('searchLabel') + ': ' + searchText;
    }

    return this.props.t('title');
  }

  render() {
    let formatDate = value => {
      return dateFormat(value, 'mmmm d');
    };

    let counter = 0;

    if (this.state.instructions == null) {
      return (
        <Spinner name="mySpinner" show={true}>
          <h1 className="text-center main-title">Načítám...</h1>
        </Spinner>
      );
    }

    let instructions = this.state.instructions.reduce(
      (result, instruction, index) => {
        let name = instruction.name.value;
        let codename = instruction.urlslug.value;
        let type = instruction.type.value;
        let imageLink =
          instruction.mainphoto.value[0].url + '?w=230&format=auto';
        let link = `/${this.props.language}/${this.props.t(
          'instructionUrlPrefix'
        )}-${instruction.urlslug.value}`;
        var imageStyle = {
          backgroundImage: 'url(' + imageLink + ')'
        };
        result.push(
          <div className="stitch-tile-container " key={counter++}>
            <Link to={link}>
              <div className="stitch-container tile">
                <div className="stitch-image" style={imageStyle} />
                <div className="stitch-content">
                  {/*<Tags data={this.props.data.tags} />*/}
                  <div className="stitch-title">
                    <div className="stitch-title-border" />
                    <span className="stitch-title-inner" title={codename}>
                      {name}
                    </span>
                  </div>
                  <div className={'stitch-type ' + type} />
                </div>
              </div>
            </Link>
          </div>
        );

        return result;
      },
      []
    );

    return (
      <div>
        <h1 className="text-center main-title">{this.getMainTitle()}</h1>
        <div className="arrow-down" onClick={this.scrollDown} />
        <div className="clearfix" />
        <div className="stitch-list-container">
          <div className="stitch-items">{instructions}</div>
        </div>
      </div>
    );
  }
}

export default translate('Global')(Instructions);
