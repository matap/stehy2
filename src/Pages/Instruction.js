import React, { Component } from 'react';
import { translate } from 'react-translate';
import Iframe from 'react-iframe';
import dateFormat from 'dateformat';
import { InstructionStore } from '../Stores/Instruction';
import RichTextElement from '../Components/RichTextElement';
import { dateFormats } from '../Utilities/LanguageCodes';
import Metadata from '../Components/Metadata';
import $ from 'jquery';
import slick from 'slick-carousel';
import mousewheel from 'jquery-mousewheel';
import DescriptionText from '../Components/DescriptionText';
import { spinnerService } from '@chevtek/react-spinners';
import Location from '../Components/Location';

let getState = props => {
  return {
    instruction: InstructionStore.getInstruction(
      props.match.params.instructionSlug,
      props.language
    )
  };
};

class Instruction extends Component {
  constructor(props) {
    super(props);

    this.state = getState(props);
    this.onChange = this.onChange.bind(this);
    this.isTypeSlideshow = this.isTypeSlideshow.bind(this);
    this.isTypeVideo = this.isTypeVideo.bind(this);
    this.isTextInstruction = this.isTextInstruction.bind(this);
    this.initSlick = this.initSlick.bind(this);
    this.setProgressBar = this.setProgressBar.bind(this);
    dateFormat.i18n = dateFormats[props.language] || dateFormats[0];

    this.stitchContainer = React.createRef();
    this.slickContainer = React.createRef();
    this.thumbnail = React.createRef();
  }

  componentDidMount() {
    InstructionStore.addChangeListener(this.onChange);
    InstructionStore.provideInstruction(
      this.props.match.params.instructionSlug,
      this.props.language
    );
  }

  componentDidUpdate() {
    var instruction = this.state.instruction;
    if (typeof instruction !== 'undefined' && instruction != null) {
      this.initSlick(instruction);
    }
  }

  componentWillUnmount() {
    InstructionStore.removeChangeListener(this.onChange);
    InstructionStore.unsubscribe();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.language !== nextProps.language)
      if (true) {
        InstructionStore.provideInstruction(
          nextProps.match.params.instructionId,
          nextProps.language
        );
        dateFormat.i18n = dateFormats[nextProps.language] || dateFormats[0];

        return {
          language: nextProps.language
        };
      }
    return null;
  }

  onChange() {
    this.setState(getState(this.props));
  }

  isTypeSlideshow(type) {
    return type !== 'undefined' && type != null && type === 'slideshow';
  }

  isTypeVideo(type) {
    return type !== 'undefined' && type != null && type === 'video';
  }

  isTextInstruction(type) {
    return type !== 'undefined' && type != null && type === 'text';
  }

  setProgressBar(value) {
    if (
      typeof this.props.spinner.current !== 'undefined' &&
      this.props.spinner.current != null
    ) {
      this.props.spinner.current.style.width = value + '%';
    }
  }

  initSlick(instruction) {
    var pad = function(num, size) {
      var s = num + '';
      while (s.length < size) s = '0' + s;
      return s;
    };

    // let slideshowImages = instruction.slideshowimages.value;
    let slideshowImagesCount = instruction.slideshowimagescount.value;
    if (slideshowImagesCount < 1) return;
    var self = this;
    $(this.thumbnail.current).empty();
    var parent = $(this.refs.stitchContainer);

    var slickElem = $('.stitch-slick', parent);
    if (slickElem.hasClass('slick-initialized')) {
      slickElem.slick('unslick');
    }
    slickElem.empty();

    var images = [];

    // var photogalleryImagesURLs = slideshowImages
    //   .sort(function(a, b) {
    //     var x = a.name.toLowerCase();
    //     var y = b.name.toLowerCase();
    //     return x < y ? -1 : x > y ? 1 : 0;
    //   })
    //   .map(asset => {
    //     return asset.url;
    //   });

    //for (var i = 0; i < photogalleryImagesURLs.length; i++) {
    for (var i = 1; i <= slideshowImagesCount; i++) {
      var quality = ''; // "?fm=jpg&auto=format&w="+ this.slickContainer.current.clientWidth;
      //var imageSrc = photogalleryImagesURLs[i] + quality;
      var imageSrc =
        window.location.protocol +
        '//remesla.mjakub.cz/stehy/' +
        instruction.codename.value +
        '/' +
        instruction.codename.value +
        '_' +
        pad(i, 5) +
        '.jpg';
      slickElem.append(
        '<div><img ' + (i < 0 ? 'src="' : 'data-lazy="') + imageSrc + '"></div>'
      );
      images.push(imageSrc);
      // $(this.thumbnail.current).append(
      //   '<div><img src="' + imageSrc + '"></div>'
      // );
    }

    self.setProgressBar(0);
    if (spinnerService.isShowing('apiSpinner') === false) {
      spinnerService.show('apiSpinner');
    }

    var count = images.length;
    var loadedImagesCount = 0;
    var preloadContainer = $(this.thumbnail.current); //$('.thumbnail');
    slickElem
      .slick({
        speed: 0,
        adaptiveHeight: false,
        autoplay: false,
        autoplaySpeed: 400,
        pauseOnHover: false,
        arrows: true,
        lazyLoad: 'progressive',
        slidesToShow: 1
      })
      .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        $('.icon-scroll-container', parent).hide();
      })
      .on('lazyLoaded', function(event, slick, image, imageSource) {
        var imageLazyLoaded = $('<img/>');
        imageLazyLoaded[0].src = imageSource;
        preloadContainer.append(imageLazyLoaded);

        loadedImagesCount++;
        self.setProgressBar((loadedImagesCount / count) * 100);

        if (loadedImagesCount === count) {
          spinnerService.hide('apiSpinner');
        }
      })
      .mousewheel(function(e) {
        e.preventDefault();
        if (e.deltaY < 0) {
          $(this).slick('slickNext');
        } else {
          $(this).slick('slickPrev');
        }
      });

    $('.play', parent).click(function() {
      $(this)
        .parent()
        .find('.stitch-slick')
        .slick('slickPlay');
    });

    $('.pause', parent).click(function() {
      $(this)
        .parent()
        .find('.stitch-slick')
        .slick('slickPause');
    });
  }

  render() {
    let instruction = this.state.instruction;

    if (!instruction) {
      return <div className="container" />;
    }

    let formatDate = value => {
      return dateFormat(value, 'dddd, mmmm d, yyyy');
    };

    let name = instruction.name.value;
    let codename = instruction.urlslug.value;
    let application = instruction.application;
    let description = instruction.description;
    let howto = instruction.howto;
    let type = instruction.type.value[0].codename;
    let imageLink = instruction.mainphoto.value[0].url;
    let link = `/${this.props.language}/instructions/${
      instruction.urlslug.value
    }`;
    //let slideshowImages = instruction.slideshowimages.value.map(a => a.url);
    let MainDataContentStructure = null;

    if (this.isTypeVideo(type)) {
      MainDataContentStructure = props => {
        return (
          <div className="pb-30">
            <div className="videoWrapper">
              <Iframe
                url={
                  'https://www.youtube.com/embed/' + instruction.youtubeid.value
                }
                width="100%"
                allowFullScreen
              />
            </div>
          </div>
        );
      };
    }
    if (this.isTypeSlideshow(type)) {
      MainDataContentStructure = props => {
        return (
          <div className="stitch-slick-container " ref={this.slickContainer}>
            <div className="stitch-slick" />
            <div className="thumbnail" ref={this.thumbnail} />
            <div className="icon-scroll-container">
              <div className="icon-scroll" />
              <div className="icon-scroll-text">scroll</div>
            </div>
            <div className="play button">
              <div className="icon">play</div>
            </div>
            <div className="pause button">
              <div className="icon">pause</div>
            </div>
          </div>
        );
      };
    }

    if (this.isTextInstruction(type)) {
      MainDataContentStructure = props => {
        return (
          <DescriptionText data={description} cssClass="application" title="" />
        );
      };
    }

    return (
      <div className="stitch-container full" ref="stitchContainer">
        <div className="container">
          <h1 className="stitch-big-title">{name}</h1>
          <div className={this.isTextInstruction(type) ? 'text-page' : ''}>
            <MainDataContentStructure />
            <DescriptionText
              data={application}
              cssClass="application"
              title={this.props.t('application')}
            />
            <DescriptionText
              data={howto}
              cssClass="howTo"
              title={this.props.t('howto')}
            />
            <Location
              language={this.props.language}
              location={instruction.location}
            />
          </div>
        </div>
        <Metadata
          title={instruction.name}
          description={instruction.description}
          ogTitle={instruction.name}
          ogImage={instruction.mainphoto}
          ogDescription={instruction.description}
          twitterTitle={instruction.name}
          twitterDescription={instruction.description}
          twitterImage={instruction.mainphoto}
        />
      </div>
    );
  }
}

export default translate('Instruction')(Instruction);
