import React from 'react';
import { ContentItem } from 'kentico-cloud-delivery';
import { resolveContentLink } from '../Utilities/ContentLinks';
import Link from '../Components/LowerCaseUrlLink';
import { languageCodes } from '../Utilities/LanguageCodes';

export class Instruction extends ContentItem {
  constructor() {
    super({
      richTextResolver: item => {
        let name = item.name.value;
        let urlslug = item.urlslug.value;
        var language = '';
        var instructionPrefix = '';
        if (
          window.location.pathname
            .toLowerCase()
            .startsWith('/' + languageCodes[1].toLowerCase())
        ) {
          language = '/' + languageCodes[1];
          instructionPrefix = 'instruction';
        } else {
          language = '/' + languageCodes[0];
          instructionPrefix = 'navod';
        }

        //return React.renderToString(<Link id="tweet">${name}</Link>);
        return `<a id="instruction-text" href="${language.toLowerCase()}/${instructionPrefix}-${urlslug}">${name}</a>`;
      },

      propertyResolver: fieldName => {
        // if (fieldName === 'url_pattern') {
        //   return 'urlPattern';
        // }
        return fieldName;
      },
      linkResolver: link => resolveContentLink(link)
    });
  }
}
