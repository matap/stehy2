import { TaxonomyGroup } from 'kentico-cloud-delivery';
import { resolveContentLink } from '../Utilities/ContentLinks';

export class Taxonomy extends TaxonomyGroup {
  constructor() {
    super({
      propertyResolver: fieldName => {
        return fieldName;
      },
      linkResolver: link => resolveContentLink(link)
    });
  }
}
