//import 'react-app-polyfill/ie9';
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import App from './LocalizedApp';
import Configuration from './Pages/Admin/Configuration';
import { CookiesProvider } from 'react-cookie';
import { projectConfigurationPath } from './Utilities/SelectedProject';
import Analytics from 'react-router-ga';

import './bootstrap.css';
import './remeslo-helper.css';
import './style.css';
import './css/slick.css';
import './css/slick-theme.css';

const application = (
  <CookiesProvider>
    <Router>
      <Analytics id="UA-109347296-1" debug>
        <Switch>
          <Route
            path={projectConfigurationPath}
            render={matchProps => <Configuration {...matchProps} />}
          />
          <Route path="/:lang" render={matchProps => <App {...matchProps} />} />
          <Route path="/" render={matchProps => <App {...matchProps} />} />
        </Switch>
      </Analytics>
    </Router>
  </CookiesProvider>
);

ReactDOM.render(application, document.getElementById('root'));
