import { Client } from '../Client.js';
import { SortOrder } from 'kentico-cloud-delivery';
import {
  ImageUrlBuilder,
  ImageCompressionEnum,
  ImageFitModeEnum
} from 'kentico-cloud-delivery';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import {
  initLanguageCodeObject,
  defaultLanguage
} from '../Utilities/LanguageCodes';
import { spinnerService } from '@chevtek/react-spinners';
import { withCookies, Cookies } from 'react-cookie';

const cookies = new Cookies(document.cookies);
let unsubscribe = new Subject();
const resetStore = () => ({
  instructionList: initLanguageCodeObject(),
  instructionDetails: initLanguageCodeObject()
});
let { instructionList, instructionDetails } = resetStore();

let changeListeners = [];

let notifyChange = () => {
  changeListeners.forEach(listener => {
    listener();
  });
};

class Instruction {
  // Actions
  provideInstruction(instructionSlug, language) {
    let query = Client.items()
      .type('instruction')
      .orderParameter('elements.order', SortOrder.asc)
      .equalsFilter('elements.urlslug', instructionSlug);

    if (language) {
      query.languageParameter(language);
    }

    if (spinnerService.isShowing('apiSpinner') === false) {
      spinnerService.show('apiSpinner');
    }

    query
      .queryConfig({
        richTextImageResolver: (image, fieldName) => {
          const newImageUrl = new ImageUrlBuilder(image.url)
            .withWidth(1200)
            .getUrl();

          return {
            url: newImageUrl
          };
        }
      })
      .getObservable()
      .pipe(takeUntil(unsubscribe))
      .subscribe(response => {
        if (!response.isEmpty) {
          if (language) {
            instructionDetails[language][instructionSlug] = response.items[0];
          } else {
            instructionDetails[defaultLanguage][instructionSlug] =
              response.items[0];
          }
          notifyChange();
        }
      });
  }

  provideInstructions(language, urlSlug, location, searchText) {
    let query = Client.items()
      .type('instruction')
      .orderParameter('elements.order', SortOrder.asc);

    if (language) {
      query.languageParameter(language);
    }

    if (
      typeof urlSlug !== 'undefined' &&
      urlSlug != null &&
      urlSlug.length > 0 &&
      urlSlug != 'cs-cz' &&
      urlSlug != 'en-us' &&
      urlSlug.indexOf('hledej') == -1
    ) {
      query.containsFilter('elements.sitemap', [urlSlug]);
    }

    if (
      typeof location !== 'undefined' &&
      location != null &&
      location.length > 0 &&
      location != 'cs-cz' &&
      location != 'en-us' &&
      location.indexOf('hledej') == -1
    ) {
      query.containsFilter('elements.location', [location]);
    }

    // if (searchText) {
    //   query.containsFilter('system.name', [searchText]);
    // }

    if (spinnerService.isShowing('apiSpinner') === false) {
      spinnerService.show('apiSpinner');
    }

    query
      .getObservable()
      .pipe(takeUntil(unsubscribe))
      .subscribe(response => {
        if (language) {
          instructionList[language] = response.items;
        } else {
          instructionList[defaultLanguage] = response.items;
        }
        notifyChange();
      });
  }

  // Methods
  getInstruction(instructionSlug, language) {
    spinnerService.hide('apiSpinner');
    if (language) {
      return instructionDetails[language][instructionSlug];
    } else {
      return instructionDetails[defaultLanguage][instructionSlug];
    }
  }

  getInstructions(count, language, instructionsSlug, searchText) {
    spinnerService.hide('apiSpinner');

    //var instructionsCache = cookie.get('instructionsCache');

    var result = null;
    if (language) {
      result = instructionList[language].slice(0, count);
    } else {
      result = instructionList[defaultLanguage].slice(0, count);
    }

    if (
      typeof searchText !== 'undefined' &&
      searchText != null &&
      searchText.length > 0
    ) {
      result = result.filter(
        a => a.name.text.toLowerCase().indexOf(searchText.toLowerCase()) >= 0
      );
    }

    // cookies.set('instructionsCache-' + searchText, JSON.stringify(result), {
    //   path: '/'
    // });
    return result;
  }

  // Listeners
  addChangeListener(listener) {
    changeListeners.push(listener);
  }

  removeChangeListener(listener) {
    changeListeners = changeListeners.filter(element => {
      return element !== listener;
    });
  }

  unsubscribe() {
    unsubscribe.next();
    unsubscribe.complete();
    unsubscribe = new Subject();
  }
}

let InstructionStore = new Instruction();

export { InstructionStore, resetStore };
