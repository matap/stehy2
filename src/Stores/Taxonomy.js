import { Client } from '../Client.js';
import { Subject } from 'rxjs';
import {
  initLanguageCodeObject,
  defaultLanguage
} from '../Utilities/LanguageCodes';
import { spinnerService } from '@chevtek/react-spinners';

let unsubscribe = new Subject();
const resetStore = () => ({
  taxonomyList: initLanguageCodeObject()
});
let { taxonomyList, taxonomyDetails } = resetStore();

let changeListeners = [];

let notifyChange = () => {
  changeListeners.forEach(listener => {
    listener();
  });
};

class Taxonomy {
  provideTaxonomy(taxonomygroup, language) {
    let query = Client.taxonomy(taxonomygroup);

    // if (language) {
    //   query.languageParameter(language);
    // }

    query.getObservable().subscribe(response => {
      if (!response.isEmpty) {
        if (language) {
          taxonomyList[language + '-' + taxonomygroup] =
            response.taxonomy.terms;
        } else {
          taxonomyList[defaultLanguage + '-' + taxonomygroup] =
            response.taxonomy.terms;
        }
        notifyChange();
      }
    });
  }

  // Methods
  getTaxonomy(taxonomygroup, language) {
    if (language) {
      return taxonomyList[language + '-' + taxonomygroup];
    } else {
      return taxonomyList[defaultLanguage + '-' + taxonomygroup];
    }
  }

  // Listeners
  addChangeListener(listener) {
    changeListeners.push(listener);
  }

  removeChangeListener(listener) {
    changeListeners = changeListeners.filter(element => {
      return element !== listener;
    });
  }

  unsubscribe() {
    unsubscribe.next();
    unsubscribe.complete();
    unsubscribe = new Subject();
  }
}

let TaxonomyStore = new Taxonomy();

export { TaxonomyStore, resetStore };
