import React from 'react';
import { englishCode, czechCode } from '../Utilities/LanguageCodes';

const LanguageSelector = props => {
  return (
    <ul className="language-selector">
      <li>
        <a
          className="en-us"
          href="/"
          onClick={e => {
            e.preventDefault();
            props.changeLanguage(englishCode);
          }}
        >
          English
        </a>
      </li>
      <li>
        <a
          className="cs-cz"
          href="/"
          onClick={e => {
            e.preventDefault();
            props.changeLanguage(czechCode);
          }}
        >
          Česky
        </a>
      </li>
    </ul>
  );
};

export default LanguageSelector;
