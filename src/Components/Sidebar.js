import React, { Component } from 'react';
import Logo from '../Images/logo.png';
import LogoEnglish from '../Images/logo-en.jpg';
import MainMenu from './MainMenu';
import Link from '../Components/LowerCaseUrlLink';
import LanguageSelector from './LanguageSelector';
import SearchBox from './SearchBox';
import { czechCode } from '../Utilities/LanguageCodes';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: this.props.filterText,
      showSidebar: this.props.showSidebar
    };

    this.handleUserInput = this.handleUserInput.bind(this);
    this.toggleSidebar = this.toggleSidebar.bind(this);
    this.onMenuClick = this.onMenuClick.bind(this);
    this.onLocationMenuClick = this.onLocationMenuClick.bind(this);
  }

  handleUserInput(filterText) {
    this.setState({
      filterText: filterText
    });

    this.props.onUpdate(filterText);
  }

  toggleSidebar() {
    this.setState({
      showSidebar: !this.props.showSidebar
    });

    this.props.onShowChange(!this.props.showSidebar);
  }

  onMenuClick(a) {
    this.handleUserInput(a);
    this.props.onMenuClick(a);
  }

  onLocationMenuClick(a) {
    this.props.onLocationMenuClick(a);
  }

  render() {
    var openClass = this.props.showSidebar ? 'open' : 'close';
    return (
      <div className="sidebar-container">
        <div
          className={openClass + ' sidebar-icon nav-icon'}
          onClick={this.toggleSidebar}
        >
          menu
        </div>
        <div className={'sidebar ' + openClass}>
          <div className="sidebar-title">
            <Link to={`/${this.props.language}`} className="logo">
              <span className="logo-border" />

              <img
                src={this.props.language == czechCode ? Logo : LogoEnglish}
                alt="řemeslo jako vyšité"
              />
            </Link>
          </div>
          <LanguageSelector
            language={this.props.language}
            changeLanguage={this.props.changeLanguage}
          />
          <SearchBox
            filterText={this.state.filterText}
            onUserInput={this.handleUserInput}
            {...this.props}
          />
          <MainMenu
            onMenuClick={this.onMenuClick}
            level={1}
            language={this.props.language}
          />
          {/*{locationControl}*/}
        </div>
      </div>
    );
  }
}

export default Sidebar;
