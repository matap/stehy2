import React, { Component } from 'react';
import MainMenu from './MainMenu';
import Link from '../Components/LowerCaseUrlLink';
import { translate } from 'react-translate';

class MainMenuItem extends Component {
  constructor(props) {
    super(props);

    this.openToggle = this.openToggle.bind(this);
    this.state = {
      isOpened: props.data.hasSubmenu ? true : false
    };
    this.onMenuClick = this.onMenuClick.bind(this);
  }

  openToggle(e) {
    this.setState({
      isOpened: !this.state.isOpened
    });
  }

  hasSubItems(data) {
    if (data != null && data !== 'undefined') {
      return data.length > 0;
    }

    return false;
  }

  onMenuClick() {
    this.props.onMenuClick();
  }

  render() {
    var data = this.props.data;
    var isOpenedCssClass = this.state.isOpened ? 'opened' : 'closed';
    var subItemsToggle = this.hasSubItems(data.terms) ? (
      <span className="open-toggle" onClick={this.openToggle} />
    ) : null;
    var menuLink = (
      <Link
        to={`/${this.props.language}/` + data.codename}
        onClick={this.onMenuClick}
      >
        {this.props.t(data.codename)}
        {/*data.name*/}
      </Link>
    );
    return (
      <li className={isOpenedCssClass}>
        {menuLink}
        {subItemsToggle}
        <MainMenu
          data={data.terms}
          level={this.props.level + 1}
          ref="submenu"
          onMenuClick={this.onMenuClick}
          language={this.props.language}
        />
      </li>
    );
  }
}
export default translate('Global')(MainMenuItem);
