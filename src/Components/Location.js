import React, { Component } from 'react';
//import { TaxonomyStore } from '../Stores/Taxonomy';
import { translate } from 'react-translate';

// let getState = props => {
//   return {
//     taxonomy: TaxonomyStore.getTaxonomy('location', props.language),
//     lastClick: ''
//   };
// };

class Location extends Component {
  // constructor(props) {
  //     super(props);

  //     this.state = getState(props);
  //     this.onChange = this.onChange.bind(this);
  //   }

  // onChange() {
  //     this.setState(getState(this.props));
  //   }

  // componentDidMount() {
  //     if (typeof this.props.data !== 'undefined' && this.props.data != null) {
  //         // do nothing
  //     } else {
  //         TaxonomyStore.addChangeListener(this.onChange);
  //         TaxonomyStore.provideTaxonomy('location', this.props.language);
  //     }
  // }

  // componentWillUnmount() {
  //     TaxonomyStore.removeChangeListener(this.onChange);
  //     TaxonomyStore.unsubscribe();
  // }

  render() {
    var cities = this.props.location.taxonomyTerms;
    if (typeof cities !== 'undefined' && cities.length > 0) {
      return (
        <div className="location">
          <div>
            <strong>{this.props.t('locationTitle')}</strong>
          </div>
          <ul>
            {cities.map((city, i) => (
              <li key={'li' + i}>{city.name}</li>
            ))}
          </ul>
        </div>
      );
    }

    return null;
  }
}

export default translate('Location')(Location);
