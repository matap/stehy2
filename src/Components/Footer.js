import React from 'react';
import Link from '../Components/LowerCaseUrlLink';
import { translate } from 'react-translate';
const Footer = props => {
  return (
    <div className="footer">
      <div className="footer-border" />
      <a href="http://www.mjakub.cz" target="_blank">
        Muzeum J. A. Komenského v Uherském Brodě
      </a>
      <div className="footer-date">| 2016 - {new Date().getFullYear()} |</div>
      <div className="clearfix" />
    </div>
  );
};
export default translate('Footer')(Footer);
