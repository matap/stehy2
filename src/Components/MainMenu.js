import React, { Component } from 'react';
import MainMenuItem from './MainMenuItem';
import { TaxonomyStore } from '../Stores/Taxonomy';

let getState = props => {
  return {
    taxonomy: TaxonomyStore.getTaxonomy('sitemap_7f828bb', props.language),
    lastClick: ''
  };
};

class MainMenu extends Component {
  constructor(props) {
    super(props);

    this.state = getState(props);
    this.onMenuClick = this.onMenuClick.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange() {
    this.setState(getState(this.props));
  }

  componentDidMount() {
    if (typeof this.props.data !== 'undefined' && this.props.data != null) {
      // do nothing
    } else {
      TaxonomyStore.addChangeListener(this.onChange);
      TaxonomyStore.provideTaxonomy('sitemap_7f828bb', this.props.language);
    }
  }

  componentWillUnmount() {
    TaxonomyStore.removeChangeListener(this.onChange);
    TaxonomyStore.unsubscribe();
  }

  onMenuClick(a) {
    this.setState({
      lastClick: !this.state.showSidebar
    });

    this.props.onMenuClick(a);
  }

  render() {
    var data = null;
    if (typeof this.props.data !== 'undefined' && this.props.data != null) {
      data = this.props.data;
    } else {
      data = this.state.taxonomy;
    }
    var isOpenedCssClass = this.state.isOpened ? 'opened' : 'closed';
    if (data != null && data !== 'undefined' && this.props.level < 4) {
      return (
        <ul
          className={
            'main-menu level-' + this.props.level + ' ' + isOpenedCssClass
          }
          key={'ul' + this.props.level}
        >
          {data.map(a => {
            if (a != null && a !== 'undefined') {
              return (
                <MainMenuItem
                  data={a}
                  level={this.props.level + 1}
                  onMenuClick={this.onMenuClick}
                  language={this.props.language}
                  key={this.props.level + a.codename}
                />
              );
            }

            return null;
          })}
        </ul>
      );
    }

    return null;
  }
}

export default MainMenu;
