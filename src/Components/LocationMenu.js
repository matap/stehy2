import React, { Component } from 'react';
import { TaxonomyStore } from '../Stores/Taxonomy';
import Link from '../Components/LowerCaseUrlLink';

let getState = props => {
  return {
    taxonomy: TaxonomyStore.getTaxonomy('location', props.language),
    lastClick: ''
  };
};

class LocationMenu extends Component {
  constructor(props) {
    super(props);

    this.state = getState(props);
    this.onMenuClick = this.onMenuClick.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange() {
    this.setState(getState(this.props));
  }

  componentDidMount() {
    TaxonomyStore.addChangeListener(this.onChange);
    TaxonomyStore.provideTaxonomy('location', this.props.language);
  }

  componentWillUnmount() {
    TaxonomyStore.removeChangeListener(this.onChange);
    TaxonomyStore.unsubscribe();
  }

  onMenuClick(a) {
    this.props.onLocationMenuClick(a);
  }

  render() {
    let data = this.state.taxonomy;
    if (data != null && data !== 'undefined') {
      var areasTag = data.map((area, i) => {
        return (
          <li className={'location-' + area.codename.toLowerCase()} key={i}>
            <Link to={'lokace-' + area.codename}>
              {area.name}
              {/*<span style={{backgroundColor: area.color}}></span>*/}
            </Link>
          </li>
        );
      });

      return <ul className="location-menu">{areasTag}</ul>;
    }

    return null;
  }
}

export default LocationMenu;
