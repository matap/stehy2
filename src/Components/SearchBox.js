import React, { Component } from 'react';
import { translate } from 'react-translate';

class SearchBox extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    var textInput = this.refs.filterTextInput.value;
    this.props.history.push(
      '/' +
        this.props.language.toLowerCase() +
        '/hledej/' +
        this.refs.filterTextInput.value
    );
    this.props.onUserInput(textInput);
  }

  componentDidMount() {}

  render() {
    return (
      <div className="search-container">
        <input
          type="text"
          placeholder={this.props.t('search')}
          value={this.props.filterText}
          ref="filterTextInput"
          onChange={this.handleChange}
        />
      </div>
    );
  }
}

export default translate('SearchBox')(SearchBox);
