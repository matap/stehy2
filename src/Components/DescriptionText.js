import React, { Component } from 'react';
import RichTextElement from '../Components/RichTextElement';

class DescriptionText extends Component {
  render() {
    var data = this.props.data;
    if (
      typeof data !== 'undefined' &&
      data.value.length > 2 &&
      data.value != '<p><br></p>'
    ) {
      return (
        <div className={this.props.cssClass}>
          <div>
            <strong>{this.props.title}</strong>
          </div>
          <RichTextElement
            className="instruction-detail-content"
            element={data}
          />
        </div>
      );
    }

    return null;
  }
}

export default DescriptionText;
